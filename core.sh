#!/bin/bash  

if [[ $1 == '1' ]]
then
    echo 'enter key'
    read key
    echo 'enter value'
    read value
    echo "$key : $value" >> "$2.txt"
fi

if [[ $1 == '2' ]]
then
    echo 'enter key'
    read key
    sed -i '' "/$key /d" "./$2.txt"
fi

if [[ $1 == '3' ]]
then
    echo 'enter key'
    read key
    awk -v max=$key '$1 == max' "$2.txt"
fi


if [[ $1 == '4' ]]
then
    echo 'enter key'
    read key
    read value

    sed -i '' "/$key /d" "./$2.txt"
    echo "$key : $value" >> "$2.txt"

fi